import { Router } from 'express';
import { ActivityModel } from '../model/activitymodel';
import { wrap } from '../util';


//map qui contien nos informations
const activityMap = new Map<number, ActivityModel>();

//set a une clé spécifique une valeur, mettre object a l'endroit ou il y a objet spécifique
activityMap.set(1, { activityId: 1, name: 'Basketball' });
activityMap.set(2, { activityId: 2, name: 'Dance folklorique' });
activityMap.set(3, { activityId: 3, name: 'Tennis' });

//On fait un incrémenteur
let nextActivityId = 4;


const activityRouter = Router();

//pour tout les appels qui sont pour une activity spécifique
activityRouter.use('/:activityId', wrap(async (req, res, next) => {
    const activity = activityMap.get(parseInt(req.params.activityId));
    if (activity === undefined) { return res.sendStatus(404); }
    req.activity = activity;
    return next();
}));

activityRouter.get('/', wrap(async (_req, res) => {
    const activities = Array.from(activityMap.values());
    return res.send(activities);
}));

//Retourne l’activité avec cet identifiant
activityRouter.get('/:activityId', wrap(async (req, res) => {
    return res.send(req.activity);
}));

//on créer l'activité
activityRouter.post('/', wrap(async (req, res) => {
    const activity: ActivityModel = req.body;
    activity.activityId = nextActivityId++;

    //On met l'activité dans notre map avec l'id
    activityMap.set(activity.activityId, activity);
    return res.send(activity);
}));


//on fait un update
activityRouter.put('/:activityId', wrap(async (req, res) => {
    const updated: ActivityModel = req.body;
    req.activity.name = updated.name;
    return res.send(req.activity);
}));
activityRouter.delete('/:activityId', wrap(async (req, res) => {
    activityMap.delete(req.activity.activityId);
    return res.sendStatus(204);
}));

export { activityRouter };
